<?php

namespace addons\sdcmenu\model;

class Product extends Model
{

    // 表名
    protected $name = 'sdcmenu_product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'category_text',
        'main_image_cdn',
        'desc_images_cdn'
    ];
    
    public function getCategoryTextAttr($value,$data)
    {
        $category = Category::find($data['category']);
        return $category ? $category['name'] : '-';
    }


    
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMainImageCdnAttr($value,$data)
    {
        return $value ? $value : (isset($data['main_image']) ? cdnurl($data['main_image'],true) : '');
    }


    public function getDescImagesCdnAttr($value,$data)
    {
        if(empty($data['desc_images']))
            return $data['desc_images'];
        $images = [];
        foreach(explode(',',$data['desc_images']) as $item){
            array_push($images,cdnurl($item,true));
        }
        return $images;
    }

}
