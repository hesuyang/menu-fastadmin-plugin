<?php
namespace addons\sdcmenu\model;

use think\Model as ThinkModel;

// 模型基类
class Model extends ThinkModel
{
    const AddonName = 'sdcmenu';
}