<?php

namespace addons\sdcmenu\model;

use app\common\model\User;

class Menu extends Model
{
    // 表名
    protected $name = 'sdcmenu_menu';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'category_text',
        'user_text',
        'main_image_cdn',
        'swiper_images_cdn'
    ];

    public function getCategoryTextAttr($value,$data)
    {
        $category = Category::find($data['category']);
        return $category ? $category['name'] : '-';
    }

    public function getUserTextAttr($value,$data)
    {
        $user = User::find($data['user_id']);
        return $user ? $user['nickname'] : '-';
    }

    public function getMainImageCdnAttr($value,$data)
    {
        return $value ? $value : (isset($data['main_image']) ? cdnurl($data['main_image'],true) : '');
    }


    public function getSwiperImagesCdnAttr($value,$data)
    {
        if(empty($data['swiper_images']))
            return $data['swiper_images'];
        $images = [];
        foreach(explode(',',$data['swiper_images']) as $item){
            array_push($images,cdnurl($item,true));
        }
        return $images;
    }
}
